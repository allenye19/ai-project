import math

import sklearn.metrics
import torch.nn.functional as F

from .quac_metrics import quac_f1
from .coqa_metrics import coqa_em, coqa_f1


class Metric:
    def __init__(self):
        self._set_name()
        self.reset()

    def _set_name(self):
        raise NotImplementedError

    def reset(self):
        self._sum = 0
        self._n = 0

    def update(self, output, batch):
        raise NotImplementedError

    @property
    def value(self):
        return self._sum / self._n if self._n else 0


class Accuracy(Metric):
    def __init__(self, key, name=None):
        self._key = key
        self._name = name
        super().__init__()

    def _set_name(self):
        if self._name:
            self.name = f'Acc({self._name})'
        else:
            self.name = 'Acc'

    def update(self, output, batch):
        prediction = output[self._key].detach().cpu()
        target = batch[self._key]
        self._sum += (prediction == target).sum().item()
        self._n += len(prediction)


class WeightedAccuracy(Metric):
    def __init__(self, key, weight):
        self._key = key
        self._weight = weight
        super().__init__()

    def _set_name(self):
        self.name = f'ACC({self._key})'

    def update(self, output, batch):
        prediction = output[self._key].detach().cpu()
        target = batch[self._key]
        for p, t in zip(prediction, target):
            if p == t:
                self._sum += self._weight[t]
            self._n += self._weight[t]


class F1(Metric):
    def __init__(self, logits_key, label_key, name=None, ignore_index=None, **kwargs):
        self._name = name
        self._logits_key = logits_key
        self._label_key = label_key
        self._ignore_index = ignore_index
        self._kwargs = kwargs
        super().__init__()

    def _set_name(self):
        if self._name:
            self.name = f'F1({self._name})'
        else:
            self.name = 'F1'

    def reset(self):
        self._y_true = []
        self._y_pred = []

    def update(self, output, batch):
        logits = output[self._logits_key].detach().cpu()
        label = batch[self._label_key]
        if self._ignore_index:
            logits = logits[label != self._ignore_index]
            label = label[label != self._ignore_index]
        if label.shape[0]:
            predictions = logits.max(dim=1)[1]
            self._y_true += label.tolist()
            self._y_pred += predictions.tolist()

    @property
    def value(self):
        return sklearn.metrics.f1_score(self._y_true, self._y_pred, **self._kwargs)


class Perplexity(Metric):
    def __init__(self, device, input_key, target_key, ignore_index=-100):
        self._device = device
        self._input_key = input_key
        self._target_key = target_key
        self._ignore_index = ignore_index
        super().__init__()

    def _set_name(self):
        self.name = f'PPl({self._target_key})'

    def update(self, output, batch):
        _input = output[self._input_key]
        target = batch[self._target_key].to(device=self._device)
        loss = F.cross_entropy(_input, target, ignore_index=self._ignore_index).item()
        self._sum += math.e ** loss
        self._n += (target != self._ignore_index).sum().item()


class ExactSpanMatch(Metric):
    def __init__(self, key):
        self._key = key
        super().__init__()

    def _set_name(self):
        self.name = 'ESM'

    def update(self, output, batch):
        for output_span, span in zip(output[self._key], batch[self._key]):
            self._n += 1
            self._sum += output_span == span


class QuACF1(Metric):
    def __init__(self, span_key, context_raw_key, context_offset_key, context_len_key,
                 all_answers_key):
        self._span_key = span_key
        self._context_raw_key = context_raw_key
        self._context_offset_key = context_offset_key
        self._context_len_key = context_len_key
        self._all_answers_key = all_answers_key
        super().__init__()

    def _set_name(self):
        self.name = 'QuAC F1'

    def update(self, output, batch):
        for span, context_raw, context_offset, context_len, all_answers in \
                zip(output[self._span_key], batch[self._context_raw_key],
                    batch[self._context_offset_key], batch[self._context_len_key],
                    batch[self._all_answers_key]):
            span_start, span_end = span
            start = context_offset[span_start]
            end = len(context_raw) \
                if span_end == context_len - 1 else context_offset[span_end + 1]
            hyp = context_raw[start:end]
            self._n += 1
            self._sum += quac_f1(hyp, all_answers)


class CoQAEM(Metric):
    def __init__(self, span_key, context_raw_key, context_offset_key, context_len_key,
                 all_answers_key):
        self._span_key = span_key
        self._context_raw_key = context_raw_key
        self._context_offset_key = context_offset_key
        self._context_len_key = context_len_key
        self._all_answers_key = all_answers_key
        super().__init__()

    def _set_name(self):
        self.name = 'CoQA EM'

    def update(self, output, batch):
        for span, context_raw, context_offset, context_len, all_answers in \
                zip(output[self._span_key], batch[self._context_raw_key],
                    batch[self._context_offset_key], batch[self._context_len_key],
                    batch[self._all_answers_key]):
            span_start, span_end = span
            start = context_offset[span_start]
            end = len(context_raw) \
                if span_end == context_len - 1 else context_offset[span_end + 1]
            hyp = context_raw[start:end]
            self._n += 1
            self._sum += coqa_em(all_answers, hyp)


class CoQAF1(Metric):
    def __init__(self, span_key, context_raw_key, context_offset_key, context_len_key,
                 all_answers_key):
        self._span_key = span_key
        self._context_raw_key = context_raw_key
        self._context_offset_key = context_offset_key
        self._context_len_key = context_len_key
        self._all_answers_key = all_answers_key
        super().__init__()

    def _set_name(self):
        self.name = 'CoQA F1'

    def update(self, output, batch):
        for span, context_raw, context_offset, context_len, all_answers in \
                zip(output[self._span_key], batch[self._context_raw_key],
                    batch[self._context_offset_key], batch[self._context_len_key],
                    batch[self._all_answers_key]):
            span_start, span_end = span
            start = context_offset[span_start]
            end = len(context_raw) \
                if span_end == context_len - 1 else context_offset[span_end + 1]
            hyp = context_raw[start:end]
            self._n += 1
            self._sum += coqa_f1(all_answers, hyp)
