import re
import string
from collections import Counter


def _normalize_answer(s):
    """Lower text and remove punctuation, storys and extra whitespace."""

    def remove_articles(text):
        regex = re.compile(r'\b(a|an|the)\b', re.UNICODE)
        return re.sub(regex, ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))


def _get_tokens(s):
    if not s:
        return []
    return _normalize_answer(s).split()


def _compute_exact(a_gold, a_pred):
    return int(_normalize_answer(a_gold) == _normalize_answer(a_pred))


def _compute_f1(a_gold, a_pred):
    gold_toks = _get_tokens(a_gold)
    pred_toks = _get_tokens(a_pred)
    common = Counter(gold_toks) & Counter(pred_toks)
    num_same = sum(common.values())
    if len(gold_toks) == 0 or len(pred_toks) == 0:
        # If either is no-answer, then F1 is 1 if they agree, 0 otherwise
        return int(gold_toks == pred_toks)
    if num_same == 0:
        return 0
    precision = 1.0 * num_same / len(pred_toks)
    recall = 1.0 * num_same / len(gold_toks)
    f1 = (2 * precision * recall) / (precision + recall)
    return f1


def coqa_em(a_gold_list, a_pred):
    em_sum = 0.0
    if len(a_gold_list) > 1:
        for i in range(len(a_gold_list)):
            # exclude the current answer
            gold_answers = a_gold_list[0:i] + a_gold_list[i + 1:]
            em_sum += max(_compute_exact(a, a_pred) for a in gold_answers)
    else:
        em_sum += max(_compute_exact(a, a_pred) for a in a_gold_list)

    return em_sum / max(1, len(a_gold_list))


def coqa_f1(a_gold_list, a_pred):
    f1_sum = 0.0
    if len(a_gold_list) > 1:
        for i in range(len(a_gold_list)):
            # exclude the current answer
            gold_answers = a_gold_list[0:i] + a_gold_list[i + 1:]
            f1_sum += max(_compute_f1(a, a_pred) for a in gold_answers)
    else:
        f1_sum += max(_compute_f1(a, a_pred) for a in a_gold_list)

    return f1_sum / max(1, len(a_gold_list))
