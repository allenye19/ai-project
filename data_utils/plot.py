import argparse
import sys
from pathlib import Path

import ipdb
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm

from common.utils import load_jsonl


matplotlib.rcParams.update({'text.color': 'white', 'axes.labelcolor': 'white'})
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 16}
matplotlib.rc('font', **font)
sns.set_style("white", {"axes.facecolor": "1", 'axes.edgecolor': '1', 'figure.facecolor': '1', 'grid.color': '1', 'patch.edgecolor': '1', 'text.color': '1', 'xtick.color': '1', 'ytick.color': '1'})


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_path', type=Path, help='Input data in jsonl format')
    parser.add_argument('output_path', type=Path, help='Plot output path')
    args = parser.parse_args()

    return vars(args)


IDENTITIES = [
    'male', 'female', 'homosexual_gay_or_lesbian', 'christian', 'jewish', 'muslim',
    'black', 'white', 'psychiatric_or_mental_illness']


def main(input_path, output_path):
    data = load_jsonl(input_path, verbose=True)
    stat = {k: [0, 0] for k in ['all'] + IDENTITIES}
    for d in tqdm(data, desc='[*] Processing', dynamic_ncols=True):
        is_toxic = float(d['target']) >= 0.5
        stat['all'][0] += 1
        stat['all'][1] += is_toxic
        for i in IDENTITIES:
            if d[i] and float(d[i]) >= 0.5:
                stat[i][0] += 1
                stat[i][1] += is_toxic
    stat = sorted([(k, v[1] / v[0]) for k, v in stat.items()], key=lambda x: x[1])
    x = [s[0] for s in stat]
    x[x.index('homosexual_gay_or_lesbian')] = 'homosexual'
    x[x.index('psychiatric_or_mental_illness')] = 'mental'
    y = [s[1] for s in stat]
    fig, (ax) = plt.subplots(figsize=(10, 4))
    sns.barplot(x=x, y=y, palette='autumn_r', ax=ax)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
    ax.set_title('Toxicity comments ratio of identities')
    ax.xaxis.label.set_color('white')
    ax.yaxis.label.set_color('white')
    fig.tight_layout()
    fig.savefig(output_path, transparent=True)


if __name__ == "__main__":
    with ipdb.launch_ipdb_on_exception():
        sys.breakpointhook = ipdb.set_trace
        kwargs = parse_args()
        main(**kwargs)
