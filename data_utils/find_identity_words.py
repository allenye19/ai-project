import argparse
import string
import sys
from collections import Counter
from pathlib import Path

import ipdb
import spacy.lang.en
from tqdm import tqdm

from common.utils import load_jsonl, save_jsonl


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('data_path', type=Path, help='Training data in jsonl format')
    parser.add_argument('module_path', type=Path, help='Output python module file')
    parser.add_argument('output_path', type=Path, help='Output file')
    args = parser.parse_args()

    return vars(args)


IDENTITIES = [
    'male', 'female', 'transgender', 'other_gender', 'heterosexual',
    'homosexual_gay_or_lesbian', 'bisexual', 'other_sexual_orientation', 'christian',
    'jewish', 'muslim', 'hindu', 'buddhist', 'atheist', 'other_religion', 'black',
    'white', 'asian', 'latino', 'other_race_or_ethnicity', 'physical_disability',
    'intellectual_or_learning_disability', 'psychiatric_or_mental_illness',
    'other_disability']
STOP_WORDS = spacy.lang.en.STOP_WORDS
STOP_WORDS -= {'he', 'his', 'him', 'himself', 'she', 'hers', 'her', 'herself'}
STOP_WORDS |= {'\'d', '\'ll', '\'m', '\'re', '\'s', '\'ve', 'n\'t'}
STOP_WORDS |= {' ', '  ', '\n', '\n\n', ' \n\n', '--', '...'}
STOP_WORDS |= set(string.punctuation)


def save_module(result, module_path):
    result = {r['identity']: r['words'] for r in result}
    s = 'IDENTITY_WORDS = {\n'
    for k, v in result.items():
        s += f'    \'{k}\': [\n'
        for vv in v:
            s += f'        {str(vv)},\n'
        s += '    ],\n'
    s += '}\n'
    with module_path.open(mode='w') as f:
        f.write(s)
    print(f'[-] Result saved as module at {module_path}')


def main(data_path, module_path, output_path):
    data = load_jsonl(data_path, verbose=True)
    result = {identity: Counter() for identity in IDENTITIES}
    for d in tqdm(data, desc='[*] Analyzing', dynamic_ncols=True):
        for identity in IDENTITIES:
            if d[identity] and float(d[identity]) > 0:
                tokens = [token.lower() for token in d['comment_text_tokenized']]
                result[identity].update(filter(lambda x: x not in STOP_WORDS, tokens))
    result = [{
        'identity': k,
        'words': v.most_common(50)
    } for k, v in result.items()]
    save_module(result, module_path)
    save_jsonl(result, output_path, verbose=True)


if __name__ == "__main__":
    with ipdb.launch_ipdb_on_exception():
        sys.breakpointhook = ipdb.set_trace
        kwargs = parse_args()
        main(**kwargs)
