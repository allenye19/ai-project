import argparse
import sys
from collections import defaultdict
from pathlib import Path

import ipdb
from tqdm import tqdm

from common.utils import load_jsonl, save_jsonl


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('data_path', type=Path, help='Training data in jsonl format')
    parser.add_argument('output_path', type=Path, help='Output file')
    args = parser.parse_args()

    return vars(args)


IDENTITIES = [
    'male', 'female', 'transgender', 'other_gender', 'heterosexual',
    'homosexual_gay_or_lesbian', 'bisexual', 'other_sexual_orientation', 'christian',
    'jewish', 'muslim', 'hindu', 'buddhist', 'atheist', 'other_religion', 'black',
    'white', 'asian', 'latino', 'other_race_or_ethnicity', 'physical_disability',
    'intellectual_or_learning_disability', 'psychiatric_or_mental_illness',
    'other_disability']


def merge(data):
    toxicity_annotator_count = sum([int(d['toxicity_annotator_count']) for d in data])
    target = sum([float(d['target']) for d in data]) / toxicity_annotator_count
    identity_annotator_count = sum([int(d['identity_annotator_count']) for d in data])
    identities = {
        i: sum([float(d[i]) if d[i] else 0 for d in data]) / identity_annotator_count if identity_annotator_count else 0
        for i in IDENTITIES
    }

    return {
        'id': data[0]['id'],
        'comment_text': data[0]['comment_text'],
        'comment_text_tokenized': data[0]['comment_text_tokenized'],
        'toxicity_annotator_count': toxicity_annotator_count,
        'target': target,
        'identity_annotator_count': identity_annotator_count,
        **identities
    }


def main(data_path, output_path):
    data = defaultdict(list)
    bar = tqdm(
        load_jsonl(data_path, verbose=True), desc='[*] Categorizing',
        dynamic_ncols=True)
    orig_size = len(bar)
    for d in bar:
        data[d['comment_text']].append(d)
    bar.close()
    data = [merge(d) for d in tqdm(data.values(), desc='[*] Merging', dynamic_ncols=True)]
    print(f'[#] Data size before/after merge: {orig_size:,}/{len(data)}')
    save_jsonl(data, output_path, verbose=True)


if __name__ == "__main__":
    with ipdb.launch_ipdb_on_exception():
        sys.breakpointhook = ipdb.set_trace
        kwargs = parse_args()
        main(**kwargs)
