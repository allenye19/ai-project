import argparse
import os
import sys
import multiprocessing as mp
from pathlib import Path

import ipdb
import spacy
from tqdm import tqdm

from common.utils import load_csv, save_jsonl


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_path', type=Path, help='Input file')
    parser.add_argument('output_path', type=Path, help='Output file')
    parser.add_argument(
        '-j', dest='n_processes', type=int, default=4,
        help='Number of parallel processes')
    args = parser.parse_args()

    return vars(args)


def tokenize(nlp, data, process_index=None, queue=None):
    if process_index is not None:
        bar = tqdm(
            data, desc=f'[*] P{os.getpid():<5d} Tokenizing', dynamic_ncols=True,
            position=process_index)
    else:
        bar = tqdm(data, desc='[*] Tokenizing', dynamic_ncols=True)
    for d in bar:
        d['comment_text_tokenized'] = [token.text for token in nlp(d['comment_text'])]
    bar.close()
    if process_index is not None:
        queue.put(data)
        return
    else:
        return data


def main(input_path, output_path, n_processes):
    print('[*] Creating SpaCy nlp...', end='')
    nlp = spacy.load('en')
    print('done')

    data = load_csv(input_path, verbose=True)
    if n_processes < 1:
        print('[!] -j should be provided with a positive integer')
        exit(1)
    elif n_processes == 1:
        data = tokenize(nlp, data)
    else:
        size = (len(data) + n_processes - 1) // n_processes
        queue = mp.Queue()
        processes = [
            mp.Process(target=tokenize, args=(nlp, data[size*i:size*(i+1)], i, queue))
            for i in range(n_processes)]
        for process in processes:
            process.start()
        for process in processes:
            process.join()
        data = []
        for _ in range(n_processes):
            data += queue.get()
    save_jsonl(data, output_path)


if __name__ == "__main__":
    with ipdb.launch_ipdb_on_exception():
        sys.breakpointhook = ipdb.set_trace
        kwargs = parse_args()
        main(**kwargs)
