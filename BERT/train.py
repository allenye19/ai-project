import argparse
import math
import random
import sys
from datetime import datetime
from pathlib import Path

import ipdb
import numpy as np
import torch
import torch.utils.data
from box import Box
from pytorch_pretrained_bert.tokenization import (
    BertTokenizer, PRETRAINED_VOCAB_POSITIONAL_EMBEDDINGS_SIZE_MAP)
from pytorch_pretrained_bert.modeling import BertForSequenceClassification
from pytorch_pretrained_bert.optimization import BertAdam
from tqdm import tqdm

from common.base_model import BaseModel
from common.base_trainer import BaseTrainer
from common.losses import CrossEntropyLoss
from common.metrics import Accuracy, F1
from common.utils import load_pkl

from .dataset import create_data_loader
from .jigsaw_score import JigsawScore


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model_dir', type=Path, help='Target model directory')
    parser.add_argument(
        '--cont', default=False, action='store_true', help='Continue training')
    args = parser.parse_args()

    return vars(args)


class Model(BaseModel):
    def _create_net_and_optim(self, net_cfg, optim_cfg):
        net = BertForSequenceClassification.from_pretrained(**net_cfg)
        net.to(device=self._device)

        parameters = list(net.named_parameters())
        no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
        grouped_parameters = [{
            'params': [p for n, p in parameters if not any(nd in n for nd in no_decay)],
            'weight_decay': 0.01
        }, {
            'params': [p for n, p in parameters if any(nd in n for nd in no_decay)],
            'weight_decay': 0.0
        }]
        optim = BertAdam(grouped_parameters, **optim_cfg.kwargs)

        return net, optim

    def save_state(self, epoch, stat, ckpt_dir):
        tqdm.write('[*] Saving model state')
        ckpt_path = ckpt_dir / 'epoch-{}.ckpt'.format(epoch)
        torch.save({
            'timestamp': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'epoch': epoch,
            'stat': stat,
            'net_state': self._net.state_dict(),
            'optim_state': self._optim.state_dict(),
            'bert_config': self._net.config.to_dict()
        }, ckpt_path)
        tqdm.write('[-] Model state saved to {}\n'.format(ckpt_path))


class Trainer(BaseTrainer):
    def _run_batch(self, batch):
        input_ids = batch['input_ids'].to(device=self._device)
        token_type_ids = batch['token_type_ids'].to(device=self._device)
        attention_mask = batch['attention_mask'].to(device=self._device)
        logits = self._model(
            input_ids, token_type_ids=token_type_ids, attention_mask=attention_mask)
        label = logits.max(dim=1)[1]

        return {
            'logits': logits,
            'label': label
        }


def main(model_dir, cont):
    try:
        cfg = Box.from_yaml(filename=model_dir / 'config.yaml')
    except FileNotFoundError:
        print(f'[!] Model directory({model_dir}) must contain config.yaml')
        exit(1)
    print(f'[-] Model checkpoints and training log will be saved to {model_dir}\n')

    device = torch.device(f'{cfg.device.type}:{cfg.device.ordinal}')
    random.seed(cfg.random_seed)
    np.random.seed(cfg.random_seed)
    torch.manual_seed(cfg.random_seed)
    torch.cuda.manual_seed_all(cfg.random_seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    log_path = model_dir / 'log.csv'
    ckpt_dir = model_dir / 'ckpts'
    if cont:
        if not all([p.exists() for p in [log_path, ckpt_dir]]):
            print('[!] Missing checkpoint directory or log')
            exit(2)
        elif ckpt_dir.exists() and not list(ckpt_dir.iterdir()):
            print('[!] Checkpoint directory is empty')
            exit(3)
        else:
            ckpt_path = sorted(
                list(ckpt_dir.iterdir()), key=lambda x: int(x.stem.split('-')[1]),
                reverse=True)[0]
            print(f'[-] Continue training from {ckpt_path}')
    else:
        if any([p.exists() for p in [log_path, ckpt_dir]]):
            print('[!] Directory already contains saved checkpoints or log')
            exit(4)
        ckpt_dir.mkdir()
        ckpt_path = None

    print(f'[*] Loading vocabs and datasets from {cfg.dataset_dir}')
    dataset_dir = Path(cfg.dataset_dir)
    train_dataset = load_pkl(dataset_dir / 'train.pkl')
    dev_dataset = load_pkl(dataset_dir / 'dev.pkl')
    train_dataset = torch.utils.data.ConcatDataset([train_dataset, dev_dataset])

    print('[*] Creating train/dev data loaders\n')
    if cfg.data_loader.batch_size % cfg.train.n_gradient_accumulation_steps != 0:
        print(
            f'[!] n_gradient_accumulation_steps({cfg.train.n_gradient_accumulation_steps})'
            f'is not a divider of batch_size({cfg.data_loader.batch_size})')
        exit(5)
    cfg.data_loader.batch_size //= cfg.train.n_gradient_accumulation_steps
    dataset_cfg = Box.from_yaml(filename=Path(cfg.dataset_dir) / 'config.yaml')
    bert_tokenizer = BertTokenizer.from_pretrained(**dataset_cfg.bert_tokenizer)
    max_len = PRETRAINED_VOCAB_POSITIONAL_EMBEDDINGS_SIZE_MAP[
        dataset_cfg.bert_tokenizer.pretrained_model_name_or_path]
    train_data_loader = create_data_loader(
        train_dataset, bert_tokenizer, max_len, **cfg.data_loader)
    dev_data_loader = create_data_loader(
        dev_dataset, bert_tokenizer, max_len, **cfg.data_loader)

    print('[*] Creating model\n')
    cfg.net = {}
    cfg.net.pretrained_model_name_or_path = \
        dataset_cfg.bert_tokenizer.pretrained_model_name_or_path
    cfg.net.num_labels = 2
    cfg.optim.kwargs.t_total = \
        math.ceil(
            len(train_data_loader.dataset) / cfg.data_loader.batch_size /
            cfg.train.n_gradient_accumulation_steps) * cfg.train.n_epochs
    model = Model(device, cfg.net, cfg.optim)

    losses = [CrossEntropyLoss(device, 'logits', 'label')]
    metrics = [Accuracy('label'), JigsawScore()]
    trainer = Trainer(
        device, cfg.train, train_data_loader, dev_data_loader, model, losses, metrics,
        log_path, ckpt_dir, ckpt_path)
    trainer.start()


if __name__ == "__main__":
    with ipdb.launch_ipdb_on_exception():
        sys.breakpointhook = ipdb.set_trace
        kwargs = parse_args()
        main(**kwargs)
