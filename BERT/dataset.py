import torch.utils.data


class Dataset(torch.utils.data.Dataset):
    def __init__(self, data):
        self._data = [{
            **d,
            'label': int(float(d['target']) >= 0.5)
        } for d in data]

    def __getitem__(self, index):
        return self._data[index]

    def __len__(self):
        return len(self._data)


IDENTITIES = [
    'male', 'female', 'homosexual_gay_or_lesbian', 'christian', 'jewish', 'muslim',
    'black', 'white', 'psychiatric_or_mental_illness']


def create_collate_fn(bert_tokenizer, max_len):
    cls_idx = bert_tokenizer.convert_tokens_to_ids(['[CLS]'])[0]
    sep_idx = bert_tokenizer.convert_tokens_to_ids(['[SEP]'])[0]
    pad_idx = bert_tokenizer.convert_tokens_to_ids(['[PAD]'])[0]

    def pad(batch, max_len, padding, depth=1):
        for i, b in enumerate(batch):
            if depth == 1:
                batch[i] = b[:max_len]
                batch[i] += [padding for _ in range(max_len - len(b))]
            elif depth == 2:
                for j, bb in enumerate(b):
                    batch[i][j] = bb[:max_len]
                    batch[i][j] += [padding] * (max_len - len(bb))

        return batch

    def truncate(seq1, seq2, max_len):
        while len(seq1) + len(seq2) > max_len:
            if len(seq1) > len(seq2):
                seq1.pop()
            else:
                seq2.pop()

    def collate_fn(samples):
        input_ids, token_type_ids = [], []
        for sample in samples:
            subtokens = sample['comment_text_subtoken']['index'][:max_len - 2]
            input_ids.append([cls_idx] + subtokens + [sep_idx])
            token_type_ids.append([0] * (len(subtokens) + 2))
        max_pad_len = max(map(len, input_ids))
        input_ids = pad(input_ids, max_pad_len, pad_idx)
        token_type_ids = pad(token_type_ids, max_pad_len, 0)

        input_ids = torch.tensor(input_ids)
        token_type_ids = torch.tensor(token_type_ids)
        attention_mask = (input_ids != pad_idx).to(dtype=torch.int64)
        label = torch.tensor([s['label'] for s in samples])

        batch = {
            'id': [s['id'] for s in samples],
            'input_ids': input_ids,
            'token_type_ids': token_type_ids,
            'attention_mask': attention_mask,
            'label': label,
            **{i: [s[i] and float(s[i]) >= 0.5 for s in samples] for i in IDENTITIES},
        }

        return batch

    return collate_fn


def create_data_loader(dataset, bert_tokenizer, max_len, n_workers, batch_size,
                       shuffle=True):
    collate_fn = create_collate_fn(bert_tokenizer, max_len)
    data_loader = torch.utils.data.DataLoader(
        dataset, num_workers=n_workers, shuffle=shuffle, batch_size=batch_size,
        collate_fn=collate_fn)

    return data_loader
