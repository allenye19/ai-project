import argparse
import logging
import random
import sys
from pathlib import Path

import ipdb
import numpy as np
from box import Box
from pytorch_pretrained_bert.tokenization import BertTokenizer
from tqdm import tqdm

from common.utils import load_jsonl, save_pkl
from identity_words import IDENTITY_WORDS

from .dataset import Dataset


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset_dir', type=Path, help='Dataset directory')
    parser.add_argument('--random_seed', type=int, default=19, help='Random seed')
    args = parser.parse_args()

    return vars(args)


IDENTITIES = [
    'male', 'female', 'transgender', 'other_gender', 'heterosexual',
    'homosexual_gay_or_lesbian', 'bisexual', 'other_sexual_orientation', 'christian',
    'jewish', 'muslim', 'hindu', 'buddhist', 'atheist', 'other_religion', 'black',
    'white', 'asian', 'latino', 'other_race_or_ethnicity', 'physical_disability',
    'intellectual_or_learning_disability', 'psychiatric_or_mental_illness',
    'other_disability']


def load_data(data_path, mask_identity_words=False, balance_labels=False, max_samples=0,
              dev_split=0, is_test=False):
    data = load_jsonl(data_path, verbose=True)
    if is_test:
        for d in data:
            d['target'] = 0.0
        return data

    if mask_identity_words:
        for d in tqdm(data, desc='[*] Masking identity', dynamic_ncols=True):
            comment_text_tokenized_masked = d['comment_text_tokenized'][:]
            for identity in IDENTITIES:
                if not d[identity] or float(d[identity]) <= 0:
                    continue
                comment_text_tokenized_masked = [
                    '[MASK]' if token in IDENTITY_WORDS[identity] else token
                    for token in comment_text_tokenized_masked]
            d['comment_text_tokenized_masked'] = comment_text_tokenized_masked

    if balance_labels:
        print('[*] Random sampling data to balance labels')
        orig_size = len(data)
        pos = [d for d in data if float(d['target']) >= 0.5]
        neg = [d for d in data if float(d['target']) < 0.5]
        print(f'[#] Positive samples: {len(pos):,}')
        print(f'[#] Negative samples: {len(neg):,}')
        size = min(len(pos), len(neg))
        if len(pos) > size:
            random.shuffle(pos)
            pos = pos[:size]
        elif len(neg) > size:
            random.shuffle(neg)
            neg = neg[:size]
        data = pos + neg
        print(f'[#] Size before/after sampling: {orig_size:,}/{len(data):,}')

    if max_samples > 0:
        print(f'[*] Truncating data to size {max_samples:,}...', end='', flush=True)
        random.shuffle(data)
        data = data[:max_samples]
        print('done')

    if dev_split < 0 or dev_split >= 1:
        raise ValueError(
            'error - load_data: dev_split should be in range [0, 1)')
    print(f'[-] {dev_split * 100}% of {data_path} will be split into dev set')
    split_idx = int(len(data) * (1 - dev_split))
    random.shuffle(data)
    train_data = data[:split_idx]
    dev_data = data[split_idx:]

    return train_data, dev_data


def tokenize(mode, data, bert_tokenizer):
    for d in tqdm(data, desc=f'[*] Tokenizing {mode} data', dynamic_ncols=True):
        if 'comment_text_tokenized_masked' in d:
            comment_text = d['comment_text_tokenized_masked']
        else:
            comment_text = d['comment_text_tokenized']
        comment_text_subtoken = []
        for token in comment_text:
            comment_text_subtoken += bert_tokenizer.wordpiece_tokenizer.tokenize(token)
        d['comment_text_subtoken'] = {
            'text': comment_text_subtoken,
            'index': bert_tokenizer.convert_tokens_to_ids(comment_text_subtoken)
        }

    return data


def create_dataset(data, dataset_dir):
    for m, d in data.items():
        print(f'[*] Creating {m} dataset')
        dataset = Dataset(d)
        print(f'[#] {m.capitalize()} dataset size: {len(dataset):,}')
        dataset_path = (dataset_dir / f'{m}.pkl')
        save_pkl(dataset, dataset_path)
        print(f'[-] {m.capitalize()} dataset saved to {dataset_path}')


def main(dataset_dir, random_seed):
    random.seed(random_seed)
    np.random.seed(random_seed)
    print(f'[-] Random seed set to {random_seed}')

    try:
        cfg = Box.from_yaml(filename=dataset_dir / 'config.yaml')
    except FileNotFoundError:
        print(f'[!] {dataset_dir} must be a directory and contains config.yaml')
        exit(1)
    print(f'[-] Datasets will be saved to {dataset_dir}\n')

    output_files = ['train.pkl', 'dev.pkl', 'test.pkl']
    if any([(dataset_dir / p).exists() for p in output_files]):
        print('[!] Directory already contains saved vocab/dataset')
        exit(2)

    train_data, dev_data = load_data(cfg.train_path, **cfg.load_train_kwargs)
    test_data = load_data(cfg.test_path, is_test=True)
    data = {
        'train': train_data,
        'dev': dev_data,
        'test': test_data
    }
    print()

    bert_tokenizer = BertTokenizer.from_pretrained(**cfg.bert_tokenizer)
    logging.getLogger('pytorch_pretrained_bert.tokenization').setLevel(logging.ERROR)
    data = {k: tokenize(k, v, bert_tokenizer) for k, v in data.items()}
    print()

    create_dataset(data, dataset_dir)


if __name__ == "__main__":
    with ipdb.launch_ipdb_on_exception():
        sys.breakpointhook = ipdb.set_trace
        kwargs = parse_args()
        main(**kwargs)
