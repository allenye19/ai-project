import numpy as np
import sklearn.metrics

from common.metrics import Metric


IDENTITIES = [
    'male', 'female', 'homosexual_gay_or_lesbian', 'christian', 'jewish', 'muslim',
    'black', 'white', 'psychiatric_or_mental_illness']


def power_mean(x, p):
    total = sum(np.power(x, p))
    return np.power(total / len(x), 1 / p)


def roc_auc(y_true, y_pred):
    try:
        return sklearn.metrics.roc_auc_score(y_true, y_pred)
    except ValueError:
        return np.nan


class JigsawScore(Metric):
    def __init__(self):
        super().__init__()

    def _set_name(self):
        self.name = 'Jigsaw'

    def reset(self):
        self._y_true = []
        self._y_pred = []
        self._identities = {identity: [] for identity in IDENTITIES}

    def update(self, output, batch):
        self._y_true += batch['label'].cpu().tolist()
        self._y_pred += output['logits'].softmax(dim=1)[:, 1].detach().cpu().tolist()
        for identity in IDENTITIES:
            self._identities[identity] += batch[identity]

    def _cal_subgroup_auc(self, identity):
        indices = [i for i, x in enumerate(self._identities[identity]) if x]
        y_true = [self._y_true[i] for i in indices]
        y_pred = [self._y_pred[i] for i in indices]

        return roc_auc(y_true, y_pred)

    def _cal_bpsn_auc(self, identity):
        indices = [
            i for i, (l, x) in enumerate(zip(self._y_true, self._identities[identity]))
            if (l == 0 and x) or (l == 1 and not x)]
        y_true = [self._y_true[i] for i in indices]
        y_pred = [self._y_pred[i] for i in indices]

        return roc_auc(y_true, y_pred)

    def _cal_bnsp_auc(self, identity):
        indices = [
            i for i, (l, x) in enumerate(zip(self._y_true, self._identities[identity]))
            if (l == 1 and x) or (l == 0 and not x)]
        y_true = [self._y_true[i] for i in indices]
        y_pred = [self._y_pred[i] for i in indices]

        return roc_auc(y_true, y_pred)

    @property
    def value(self):
        overall_auc = roc_auc(self._y_true, self._y_pred)
        subgroup_auc = power_mean([self._cal_subgroup_auc(i) for i in IDENTITIES], -5)
        bpsn_auc = power_mean([self._cal_bpsn_auc(i) for i in IDENTITIES], -5)
        bnsp_auc = power_mean([self._cal_bnsp_auc(i) for i in IDENTITIES], -5)
        score = np.mean([overall_auc, subgroup_auc, bpsn_auc, bnsp_auc])

        return f'{overall_auc:.5}/{subgroup_auc:.5}/{bpsn_auc:.5}/{bnsp_auc:.5}/{score:.5}'
