import argparse
import random
import sys
from pathlib import Path

import ipdb
import numpy as np
import torch
from box import Box
from pytorch_pretrained_bert.tokenization import (
    BertTokenizer, PRETRAINED_VOCAB_POSITIONAL_EMBEDDINGS_SIZE_MAP)
from pytorch_pretrained_bert.modeling import BertForSequenceClassification, BertConfig
from tqdm import tqdm

from .dataset import create_data_loader
from common.utils import load_pkl, save_csv


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model_dir', type=Path, help='Model directory')
    parser.add_argument('epoch', type=int, help='Model checkpoint number')
    parser.add_argument('--batch_size', type=int, help='Inference batch size')
    args = parser.parse_args()

    return vars(args)


def main(model_dir, epoch, batch_size):
    try:
        cfg = Box.from_yaml(filename=model_dir / 'config.yaml')
    except FileNotFoundError:
        print('[!] Model directory({}) must contain config.yaml'.format(model_dir))
        exit(1)

    device = torch.device('{}:{}'.format(cfg.device.type, cfg.device.ordinal))
    random.seed(cfg.random_seed)
    np.random.seed(cfg.random_seed)
    torch.manual_seed(cfg.random_seed)
    torch.cuda.manual_seed_all(cfg.random_seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    dataset_path = Path(cfg.dataset_dir) / 'test.pkl'
    ckpt_path = model_dir / 'ckpts' / 'epoch-{}.ckpt'.format(epoch)
    print('[-] Test dataset: {}'.format(dataset_path))
    print('[-] Model checkpoint: {}\n'.format(ckpt_path))

    print('[*] Creating data loader...', end='')
    dataset_cfg = Box.from_yaml(filename=Path(cfg.dataset_dir) / 'config.yaml')
    bert_tokenizer = BertTokenizer.from_pretrained(**dataset_cfg.bert_tokenizer)
    max_len = PRETRAINED_VOCAB_POSITIONAL_EMBEDDINGS_SIZE_MAP[
        dataset_cfg.bert_tokenizer.pretrained_model_name_or_path]
    if batch_size:
        cfg.data_loader.batch_size = batch_size
    cfg.data_loader.batch_size //= cfg.train.n_gradient_accumulation_steps
    dataset = load_pkl(dataset_path)
    data_loader = create_data_loader(
        dataset, bert_tokenizer, max_len, **cfg.data_loader, shuffle=False)
    print('done')

    print('[*] Creating model...', end='')
    ckpt = torch.load(ckpt_path)
    bert_config = BertConfig.from_dict(ckpt['bert_config'])
    model = BertForSequenceClassification(bert_config, num_labels=2)
    model.load_state_dict(ckpt['net_state'])
    model.to(device=device)
    print('done')

    prediction = predict(device, data_loader, model)
    prediction_path = model_dir / 'predictions' / f'epoch-{epoch}.csv'
    if not prediction_path.parent.exists():
        prediction_path.parent.mkdir()
        print('[-] Predictions directory {} created'.format(prediction_path.parent))
    save_csv(prediction, ['id', 'prediction'], prediction_path, verbose=True)


def predict(device, data_loader, model):
    model.eval()
    prediction = []
    with torch.no_grad():
        bar = tqdm(data_loader, desc='[*] Predict', leave=False)
        for batch in bar:
            input_ids = batch['input_ids'].to(device=device)
            token_type_ids = batch['token_type_ids'].to(device=device)
            attention_mask = batch['attention_mask'].to(device=device)
            logits = model(
                input_ids, token_type_ids=token_type_ids, attention_mask=attention_mask)
            prediction += [{
                'id': _id,
                'prediction': l.softmax(dim=0)[1].item()
            } for _id, l in zip(batch['id'], logits.cpu())]
        bar.close()

    return prediction


if __name__ == "__main__":
    with ipdb.launch_ipdb_on_exception():
        sys.breakpointhook = ipdb.set_trace
        kwargs = parse_args()
        main(**kwargs)
