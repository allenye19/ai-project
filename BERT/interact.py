import argparse
import random
import sys
import readline
from pathlib import Path

import ipdb
import numpy as np
import torch
from box import Box
from colored import fore, back, style
from pytorch_pretrained_bert.tokenization import (
    BertTokenizer, PRETRAINED_VOCAB_POSITIONAL_EMBEDDINGS_SIZE_MAP)
from pytorch_pretrained_bert.modeling import BertForSequenceClassification, BertConfig


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model_dir', type=Path, help='Model directory')
    parser.add_argument('epoch', type=int, help='Model checkpoint number')
    args = parser.parse_args()

    return vars(args)


def main(model_dir, epoch):
    try:
        cfg = Box.from_yaml(filename=model_dir / 'config.yaml')
    except FileNotFoundError:
        print('[!] Model directory({}) must contain config.yaml'.format(model_dir))
        exit(1)

    device = torch.device('{}:{}'.format(cfg.device.type, cfg.device.ordinal))
    random.seed(cfg.random_seed)
    np.random.seed(cfg.random_seed)
    torch.manual_seed(cfg.random_seed)
    torch.cuda.manual_seed_all(cfg.random_seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    ckpt_path = model_dir / 'ckpts' / 'epoch-{}.ckpt'.format(epoch)
    print('[-] Model checkpoint: {}'.format(ckpt_path))
    print('[*] Creating model...', end='', flush=True)
    ckpt = torch.load(ckpt_path)
    bert_config = BertConfig.from_dict(ckpt['bert_config'])
    model = BertForSequenceClassification(bert_config, num_labels=2)
    model.load_state_dict(ckpt['net_state'])
    model.to(device=device)
    print('done')

    dataset_cfg = Box.from_yaml(filename=Path(cfg.dataset_dir) / 'config.yaml')
    bert_tokenizer = BertTokenizer.from_pretrained(**dataset_cfg.bert_tokenizer)
    max_len = PRETRAINED_VOCAB_POSITIONAL_EMBEDDINGS_SIZE_MAP[
        dataset_cfg.bert_tokenizer.pretrained_model_name_or_path]

    while True:
        try:
            text = input('\n[~] Input a comment: ')
        except EOFError:
            break
        if text == '':
            print('[@] Comment cannot be empty')
            continue
        text = ['[CLS]'] + bert_tokenizer.tokenize(text)[:max_len - 2] + ['[SEP]']
        input_ids = bert_tokenizer.convert_tokens_to_ids(text)
        token_type_ids = [0] * len(input_ids)

        input_ids = torch.tensor([input_ids], device=device)
        token_type_ids = torch.tensor([token_type_ids], device=device)
        attention_mask = (input_ids != 0).to(dtype=torch.int64, device=device)
        model.eval()
        with torch.no_grad():
            logits = model(
                input_ids, token_type_ids=token_type_ids, attention_mask=attention_mask)
        probs = logits[0].softmax(dim=0)
        max_id = probs.argmax().item()
        if max_id == 1:
            result = fore.WHITE + back.DARK_ORANGE_3B + style.BOLD + 'TOXIC' + style.RESET
        else:
            result = fore.WHITE + back.LIGHT_SEA_GREEN + style.BOLD + 'NON-TOXIC' + style.RESET
        conf = fore.BLUE + style.BOLD + f'{probs[max_id]*100:.3f}%' + style.RESET
        print(f'[#] This comment is {result}. (confidence: {conf})')


if __name__ == "__main__":
    with ipdb.launch_ipdb_on_exception():
        sys.breakpointhook = ipdb.set_trace
        kwargs = parse_args()
        main(**kwargs)
